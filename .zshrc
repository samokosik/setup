#powerlevel10k
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# source ~/powerlevel10k/powerlevel10k.zsh-theme
source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.zsh/.p10k.zsh.
[[ ! -f ~/.zsh/.p10k.zsh ]] || source ~/.zsh/.p10k.zsh



# zsh-autosuggestions
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh



# autojump
[[ -s /home/samo/.autojump/etc/profile.d/autojump.sh ]] && source /home/samo/.autojump/etc/profile.d/autojump.sh

autoload -U compinit && compinit -u



# history
HISTFILE=/home/samo/.zsh/zsh_history
HISTSIZE=1000
SAVEHIST=2000
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTFILE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
#setopt share_history         # share command history data

# force zsh to show complete history
alias history="history 0"



# aliases
alias pacman="sudo pacman"
alias aura="sudo aura"
alias "i3config"="micro /home/samo/.config/i3/config"
alias cls="clear"
alias i3blocksconfig="micro /home/samo/.config/i3/i3blocks.conf"
alias la="ls -a"
alias lal="ls -al"
alias cdh="cd /home/samo"
alias rmr="rm -r"
alias rmrf="rm -rf"
alias dfh="df -h"
alias dfH="df -H"
alias picomconfig="micro /home/samo/.config/picom.conf"
alias zshconfig="micro .zshrc"
alias errors="journalctl -p 3 -xb"
alias ls="ls --color"
alias rickpactor="/home/samo/Downloads/Programs/Rickpactor-1.0.2.AppImage"

# if tmux is executable, X is running, and not inside a tmux session, then try to attach.
# if attachment fails, start a new session
if [ -x "$(command -v tmux)" ] && [ -n "${DISPLAY}" ]; then
  [ -z "${TMUX}" ] && { tmux attach || tmux; } >/dev/null 2>&1
fi

# zsh-syntax-highlighting
# KEEP AT THE BOTTOM
(( ${+ZSH_HIGHLIGHT_STYLES} )) || typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[path]=none
ZSH_HIGHLIGHT_STYLES[path_prefix]=none

# source ./zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
